$webResourceManager.requireResourcesForContext("jira.workflow.sharing.find.bundles")
<html>
<head>
    <title>$i18n.getText("wfshare.section.title.select")</title>
    <meta name="decorator" content="panel-admin"/>
</head>
<body class="aui-page-focused aui-page-focused-large">
    <header class="aui-page-header">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>$i18n.getText("wfshare.section.title.select")</h1>
            </div>
            #if($isSysAdmin)
                <div class="aui-page-header-actions">
                    <div class="aui-buttons">
                        <a href="#workflow-sharing-container" id="workflow-source-upm" class="aui-button workflow-source" aria-pressed="true">$i18n.getText("wfshare.import.screen.bundle.from-marketplace")</a>
                        <a href="#upload-container" id="workflow-source-upload" class="aui-button workflow-source">$i18n.getText("wfshare.import.screen.bundle.from-computer")</a>
                    </div>
                </div>
            #end
        </div>
    </header>

    #if($errorMessage)
        <div class="aui-message error">
            <div class="title">
                <span class="aui-icon icon-error"></span>
                <strong>$i18n.getText("wfshare.error.screen.error")</strong>
            </div>
            <p>$errorMessage</p>
        </div>
    #end

    #if($isSysAdmin)
        <form class="aui hidden" id="upload-container" action="$nextUrl" method="post" enctype="multipart/form-data">
            <div class="field-group">
                <label for="wfShareImportFile">$i18n.getText("wfshare.import.screen.bundle.from-computer.file")</label>
                <input class="upfile" type="file" name="wfShareImportFile" id="wfShareImportFile"/>
            </div>
            <div class="hidden">
                <input type="hidden" name="projectId" value="$projectId" />
                <input type="hidden" name="schemeId" value="$schemeId" />
            </div>

            <div class="buttons-container">
                <div class="buttons">
                    <input class="aui-button aui-button-primary" id="nextButton" disabled="disabled" type="submit" value="$i18n.getText("wfshare.button.label.next")"/>
                    <a class="cancel" href="$cancelUrl">$i18n.getText("wfshare.button.label.cancel")</a>
                </div>
            </div>
        </form>
    #end

    <div id="workflow-sharing-container">
        <form class="aui ajs-dirty-warning-exempt" id="mpac-selector">
            <fieldset>
                <div class="field-group">
                    <label for="mpac-filter">$i18n.getText("wfshare.filter.title")</label>
                    <input class="text long-field" type="text" id="mpac-filter" name="mpac-filter" title="marketplace filter">

                    <div class="description">$i18n.getText("wfshare.filter.description")</div>
                </div>
            </fieldset>
        </form>

        <div class="workflow-sharing-list-container">

            <div id="workflowBundleListContainer" class="workflow-sharing-list hidden"></div>

            <div class="workflow-sharing-loading-container">
                <aui-spinner size="small"></aui-spinner> $i18n.getText('wfshare.import.screen.bundle.loading.bundles')
            </div>

            <div class="workflow-sharing-see-more-container">
                <button class="aui-button workflow-sharing-see-more hidden">
                    <span>$i18n.getText('wfshare.import.screen.bundle.see.more')</span>
                </button>
            </div>

            <div id="workflow-sharing-message-bar"></div>

            <input type="hidden" id="projectId" value="$projectId" />
            <input type="hidden" id="schemeId" value="$schemeId" />
        </div>

        <div id="workflow-search-no-results" class="aui-message hidden">
            <p class="title">
                <strong>$i18n.getText('wfshare.filter.notfound.title')</strong>
            </p>
            <p>$i18n.getText('wfshare.filter.notfound.description')</p>
        </div>

        <div class="buttons-container">
            <a class="cancel" href="$cancelUrl">$i18n.getText("wfshare.button.label.return")</a>
        </div>
    </div>

    <input type="hidden" id="wfshareNextFormUrl" value="$nextUrl" />

    <script>
        JIRA.WSP.Analytics.sendEvent("import.step", {
            stepName: "select-bundle"
        });

        JIRA.WSP.Analytics.sendEvent("import.origin", {
            origin: "$cancelUrl".substring("$cancelUrl".lastIndexOf('/') + 1)
        });
    </script>
</body>
</html>
