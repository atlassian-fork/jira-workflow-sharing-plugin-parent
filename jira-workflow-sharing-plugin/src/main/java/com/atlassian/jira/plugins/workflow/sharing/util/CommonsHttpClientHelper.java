package com.atlassian.jira.plugins.workflow.sharing.util;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.impl.HttpHelper;
import com.google.common.collect.Multimap;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.util.EntityUtils.consumeQuietly;

public class CommonsHttpClientHelper implements HttpHelper
{
    @Override
    public Response get(URI uri) throws MpacException
    {
        HttpGet method = new HttpGet(uri);
        return executeMethod(method);
    }

    @Override
    public Response postParams(URI uri, Multimap<String, String> params) throws MpacException
    {
        HttpPost method = new HttpPost(uri);
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> param: params.entries())
        {
            formParams.add(new BasicNameValuePair(param.getKey(), param.getValue()));
        }
        try
        {
            method.setEntity(new UrlEncodedFormEntity(formParams));
        }
        catch (UnsupportedEncodingException e)
        {
            throw new MpacException(e);
        }
        return executeMethod(method);
    }

    @Override
    public Response put(URI uri, byte[] content) throws MpacException
    {
        HttpPut method = new HttpPut(uri);
        method.setEntity(new ByteArrayEntity(content, APPLICATION_JSON));
        return executeMethod(method);
    }

    @Override
    public Response delete(URI uri) throws MpacException
    {
        HttpDelete method = new HttpDelete(uri);
        return executeMethod(method);
    }

    private ResponseImpl executeMethod(HttpRequestBase method) throws MpacException
    {
        try
        {
            return new ResponseImpl(method);
        }
        catch (SocketException e)
        {
            throw new MpacException.ConnectionFailure(e);
        }
        catch (IOException e)
        {
            throw new MpacException(e);
        }
    }

    private static class ResponseImpl implements Response
    {
        private HttpResult result;
        private HttpResponse response;

        public ResponseImpl(HttpRequestBase method) throws IOException
        {
            try
            {
                this.result = HttpUtils.execute(method);
                this.response = result.getResponse();
            }
            catch (IOException e)
            {
                if (result != null)
                {
                    result.close();
                }

                throw e;
            }
        }

        public int getStatus()
        {
            return response.getStatusLine().getStatusCode();
        }

        public InputStream getContentStream() throws MpacException
        {
            try
            {
                return response.getEntity().getContent();
            }
            catch (IOException e)
            {
                throw new MpacException(e);
            }
        }

        public boolean isEmpty()
        {
            Header h = response.getFirstHeader("Content-Length");
            return (h != null) && (h.getValue().trim().equals("0"));
        }

        public void close()
        {
            if (response.getEntity() != null)
            {
                consumeQuietly(response.getEntity());
            }

            result.close();
        }
    }
}
