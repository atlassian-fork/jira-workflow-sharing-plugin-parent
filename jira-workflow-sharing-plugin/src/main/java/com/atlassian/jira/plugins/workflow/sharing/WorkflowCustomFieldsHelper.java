package com.atlassian.jira.plugins.workflow.sharing;

import java.io.IOException;

public interface WorkflowCustomFieldsHelper
{
    public static final String CUSTOM_FIELD_PREFIX = "customfield_";

    String getCustomFieldTypeClassname(String customFieldId);

    String getCustomFieldsJson(Iterable<String> fields) throws IOException;

    boolean isFromPlugin(String fieldId);

    String getPluginRelatedClassName(String fieldId);
}
