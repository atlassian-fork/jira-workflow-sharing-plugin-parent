package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;

import java.io.IOException;
import java.net.URISyntaxException;

public interface PacBundleDownloader
{
    WorkflowBundle downloadBundle(String downloadUri) throws IOException, URISyntaxException, ValidationException;
}
