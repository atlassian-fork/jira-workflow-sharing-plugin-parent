package com.atlassian.jira.plugins.workflow.sharing.pac;

public interface JWSPacClientFactory
{
    JWSPacClient getPacClient();
}
