package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.StatusInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public interface WorkflowBundle extends Serializable
{
    enum BundleSource { MPAC, MANUAL }

    BundleSource getSource();
    String getWorkflowXml();
    String getLayout();
    String getLayoutV2();
    String getAnnotations();
    Iterable<WorkflowExtensionsPluginInfo> getPluginInfo();
    Iterable<CustomFieldInfo> getCustomFieldInfo();
    Iterable<StatusInfo> getStatusInfo();
    Iterable<ScreenInfo> getScreenInfo();
    String getNotes();

    public interface Factory
    {
        WorkflowBundle bundle(InputStream stream, BundleSource source) throws IOException, ValidationException;
    }
}
