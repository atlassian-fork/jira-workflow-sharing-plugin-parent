package com.atlassian.jira.plugins.workflow.sharing.servlet;

public class ValidationException extends Exception
{
    public ValidationException(String message)
    {
        super(message);
    }
}
