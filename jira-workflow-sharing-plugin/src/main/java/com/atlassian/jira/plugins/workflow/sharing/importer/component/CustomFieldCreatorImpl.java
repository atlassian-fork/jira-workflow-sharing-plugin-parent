package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.OptionInfo;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExportAsService
@Component
public class CustomFieldCreatorImpl implements CustomFieldCreator
{
    private final CustomFieldManager customFieldManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;
    private final OptionsManager optionsManager;

    @Autowired
    public CustomFieldCreatorImpl(CustomFieldManager customFieldManager, FieldConfigSchemeManager fieldConfigSchemeManager, OptionsManager optionsManager)
    {
        this.customFieldManager = customFieldManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
        this.optionsManager = optionsManager;
    }

    @Override
    public CustomField createCustomField(CustomFieldInfo fieldInfo) throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(fieldInfo.getTypeModuleKey());
        CustomFieldSearcher searcher = null;
        
        if(StringUtils.isNotBlank(fieldInfo.getSearcherModuleKey()))
        {
            searcher = customFieldManager.getCustomFieldSearcher(fieldInfo.getSearcherModuleKey());
        }
        
        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        CustomField customField = customFieldManager.createCustomField(
                fieldInfo.getName()
                , fieldInfo.getDescription()
                , cfType
                , searcher
                , Arrays.asList(GlobalIssueContext.getInstance())
                , issueTypes);

        if (fieldInfo.getOptions() != null)
        {
            IssueContext issueContext = GlobalIssueContext.getInstance();
            FieldConfig fieldConfig = customField.getRelevantConfig(issueContext);

            for (OptionInfo optionInfo : fieldInfo.getOptions())
            {
                addOptions(optionInfo, null, fieldConfig);
            }
        }

        return customField;
    }

    private void addOptions(OptionInfo optionInfo, Long parentOptionId, FieldConfig fieldConfig)
    {
        Option option = optionsManager.createOption(fieldConfig, parentOptionId, Long.valueOf(optionInfo.getSequence()), optionInfo.getValue());

        if (option.getChildOptions() != null)
        {
            for (OptionInfo childOptionInfo : optionInfo.getChildOptions())
            {
                addOptions(childOptionInfo, option.getOptionId(), fieldConfig);
            }
        }
    }

    @Override
    public void removeCustomFieldAndSchemes(CustomField field) throws RemoveException
    {
        List<FieldConfigScheme> schemes = field.getConfigurationSchemes();
        
        for(FieldConfigScheme scheme : schemes)
        {
            fieldConfigSchemeManager.removeFieldConfigScheme(scheme.getId());
        }
        
        customFieldManager.removeCustomField(field);
    }
}
