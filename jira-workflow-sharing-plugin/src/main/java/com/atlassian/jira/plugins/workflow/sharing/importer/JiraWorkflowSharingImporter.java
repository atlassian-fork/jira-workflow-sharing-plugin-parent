package com.atlassian.jira.plugins.workflow.sharing.importer;

import com.atlassian.jira.workflow.JiraWorkflow;

public interface JiraWorkflowSharingImporter
{
    JiraWorkflow importWorkflow(SharedWorkflowImportPlan plan);
}
