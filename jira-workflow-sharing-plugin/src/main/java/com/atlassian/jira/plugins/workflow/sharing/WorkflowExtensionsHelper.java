package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.Plugin;

import com.google.common.collect.Multimap;

public interface WorkflowExtensionsHelper
{
    Multimap<Plugin, String> getRequiredPlugins(JiraWorkflow workflow);

    JiraWorkflow getWorkflowForNameAndMode(String name, String mode);

    void removeIllegalComponents(JiraWorkflow jiraWorkflow);

    JiraWorkflow copyAndRemoveIllegalComponents(JiraWorkflow workflow);

    RemovedItems getRemovedItems(JiraWorkflow workflow, boolean includeCustomFields);
}