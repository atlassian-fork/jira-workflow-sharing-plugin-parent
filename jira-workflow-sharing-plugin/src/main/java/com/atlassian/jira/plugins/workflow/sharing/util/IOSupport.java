package com.atlassian.jira.plugins.workflow.sharing.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOSupport
{
    private static final String CHARSET = "UTF-8";

    private IOSupport() {}

    public static String readString(InputStream stream) throws IOException
    {
        return IOUtils.toString(stream, CHARSET);
    }

    public static void writeString(String data, OutputStream stream) throws IOException
    {
        IOUtils.write(data, stream, CHARSET);
    }
}
