package com.atlassian.jira.plugins.workflow.sharing.model;

import com.google.common.base.Function;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.type.TypeReference;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class ScreenInfo implements Serializable
{
    private static final long serialVersionUID = 5428739985963548119L;
    public static final TypeReference<List<ScreenInfo>> LIST_TYPE = new TypeReference<List<ScreenInfo>>(){};
    public static Function<ScreenInfo, Long> GET_ORIGINAL_ID = new Function<ScreenInfo, Long>()
    {
        @Override
        public Long apply(final ScreenInfo input)
        {
            return input.getOriginalId();
        }
    };

    public static Comparator<ScreenInfo> ORDER_NAME = new Comparator<ScreenInfo>()
    {
        @Override
        public int compare(ScreenInfo o1, ScreenInfo o2)
        {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    };

    private final Long originalId;
    private final String name;
    private final String description;
    private final List<ScreenTabInfo> tabs;

    @JsonCreator
    public ScreenInfo(@JsonProperty("originalId") Long originalId
            ,@JsonProperty("name") String name
            ,@JsonProperty("description") String description
            ,@JsonProperty("tabs") List<ScreenTabInfo> tabs)
    {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.tabs = tabs;
    }

    public Long getOriginalId()
    {
        return originalId;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public List<ScreenTabInfo> getTabs()
    {
        return tabs;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(name).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof ScreenInfo)) { return false; }

        ScreenInfo info = (ScreenInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(), info.getOriginalId()).append(this.getName(), info.getName()).isEquals();
    }

    @Override
    public String toString()
    {
        return "ScreenInfo name:" + name;//NON-NLS
    }
}
