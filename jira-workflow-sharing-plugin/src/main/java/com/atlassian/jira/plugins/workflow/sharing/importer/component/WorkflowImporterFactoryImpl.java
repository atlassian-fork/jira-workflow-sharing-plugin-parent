package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowLayoutKeyFinder;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowScreensHelper;
import com.atlassian.jira.plugins.workflow.sharing.importer.JiraWorkflowSharingImporter;
import com.atlassian.jira.plugins.workflow.sharing.importer.JiraWorkflowSharingImporterImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.I18nResolver;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class WorkflowImporterFactoryImpl implements WorkflowImporterFactory
{
    private final WorkflowStatusHelper statusHelper;
    private final WorkflowManager workflowManager;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final CustomFieldCreator customFieldCreator;
    private final ScreenCreator screenCreator;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final I18nResolver i18n;
    private final WorkflowLayoutKeyFinder workflowLayoutKeyFinder;
    private final PluginAccessor pluginAccessor;

    @Autowired
    public WorkflowImporterFactoryImpl(WorkflowStatusHelper statusHelper, WorkflowManager workflowManager, WorkflowExtensionsHelper workflowExtensionsHelper,
                                       JiraAuthenticationContext jiraAuthenticationContext, WorkflowDesignerPropertySet workflowDesignerPropertySet, CustomFieldCreator customFieldCreator,
                                       ScreenCreator screenCreator, WorkflowScreensHelper workflowScreensHelper, I18nResolver i18n, WorkflowLayoutKeyFinder workflowLayoutKeyFinder,
                                       PluginAccessor pluginAccessor)
    {
        this.statusHelper = statusHelper;
        this.workflowManager = workflowManager;
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.customFieldCreator = customFieldCreator;
        this.screenCreator = screenCreator;
        this.workflowScreensHelper = workflowScreensHelper;
        this.i18n = i18n;
        this.workflowLayoutKeyFinder = workflowLayoutKeyFinder;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public JiraWorkflowSharingImporter newImporter()
    {
        return new JiraWorkflowSharingImporterImpl(statusHelper, workflowManager, workflowExtensionsHelper,
                jiraAuthenticationContext, workflowDesignerPropertySet, customFieldCreator, screenCreator, workflowScreensHelper,
                i18n, workflowLayoutKeyFinder, pluginAccessor);
    }
}
