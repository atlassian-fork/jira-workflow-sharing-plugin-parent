package com.atlassian.jira.plugins.workflow.sharing.file;

import com.google.common.collect.Maps;

import java.util.Map;

public class FileNameMapper
{
    private final Map<String, String> MAPPING = Maps.newConcurrentMap();

    public void addFile(String actualName, String nameToDisplay)
    {
        MAPPING.put(actualName, nameToDisplay);
    }

    public String getNameToDisplay(String actualName)
    {
        return MAPPING.get(actualName);
    }

    public void clearFile(String actualName)
    {
        MAPPING.remove(actualName);
    }

    public void clearAllFiles()
    {
        MAPPING.clear();
    }
}
