package com.atlassian.jira.plugins.workflow.sharing;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;

import java.util.Collection;
import java.util.Set;

public class RemovedItems
{
    private final Collection<ActionRemovedItems> allActionRemovedItems = Lists.newArrayList();

    void add(ActionRemovedItems actionRemovedItems)
    {
        allActionRemovedItems.add(actionRemovedItems);
    }

    public boolean isEmpty()
    {
        for (ActionRemovedItems actionRemovedItems : allActionRemovedItems)
        {
            if (!actionRemovedItems.isEmpty())
            {
                return false;
            }
        }
        return true;
    }

    public Collection<ActionRemovedItems> getActionRemovedItems()
    {
        return allActionRemovedItems;
    }


    public static class ActionRemovedItems
    {
        private final ActionDescriptor action;

        private final Set<String> functions = Sets.newHashSet();
        private final Set<String> validators = Sets.newHashSet();
        private final Set<String> conditions = Sets.newHashSet();
        private final Set<String> customFields = Sets.newHashSet();

        private boolean replacedEventId;

        public ActionRemovedItems(ActionDescriptor action)
        {
            this.action = action;
        }

        public ActionDescriptor getAction()
        {
            return action;
        }

        void addFunctionClass(String function)
        {
            functions.add(function);
        }

        void addValidatorClass(String validator)
        {
            validators.add(validator);
        }

        void addConditionClass(String condition)
        {
            conditions.add(condition);
        }

        void addCustomFieldClass(String customField)
        {
            customFields.add(customField);
        }

        public Set<String> getFunctions()
        {
            return functions;
        }

        public Set<String> getValidators()
        {
            return validators;
        }

        public Set<String> getConditions()
        {
            return conditions;
        }

        public Set<String> getCustomFields()
        {
            return customFields;
        }

        public boolean isReplacedEventId()
        {
            return replacedEventId;
        }

        public void markReplacedEventId()
        {
            this.replacedEventId = true;
        }

        public boolean isEmpty()
        {
            return functions.isEmpty() && validators.isEmpty() && conditions.isEmpty() && customFields.isEmpty() && !replacedEventId;
        }
    }
}
