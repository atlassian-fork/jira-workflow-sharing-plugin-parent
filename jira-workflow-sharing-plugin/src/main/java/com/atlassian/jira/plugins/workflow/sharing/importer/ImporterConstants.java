package com.atlassian.jira.plugins.workflow.sharing.importer;

public class ImporterConstants
{
    public static final long MAX_STREAM = 5 * 1024 * 1024;
    public static final long MAX_ENTRY_BYTES = 1024 * 1024;

    private ImporterConstants() {}
}
