package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.importer.JiraWorkflowSharingImporter;

public interface WorkflowImporterFactory
{
    JiraWorkflowSharingImporter newImporter();
}
