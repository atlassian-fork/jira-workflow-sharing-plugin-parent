package com.atlassian.jira.plugins.workflow.sharing;

public interface WhitelistChecker
{
    boolean isAllowed(String className);
}
