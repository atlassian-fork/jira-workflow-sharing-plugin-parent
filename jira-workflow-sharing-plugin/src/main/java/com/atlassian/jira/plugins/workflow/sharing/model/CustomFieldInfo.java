package com.atlassian.jira.plugins.workflow.sharing.model;

import com.google.common.base.Function;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.type.TypeReference;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomFieldInfo implements Serializable
{
    public static TypeReference<List<CustomFieldInfo>> LIST_TYPE = new TypeReference<List<CustomFieldInfo>>(){};
    public static Function<CustomFieldInfo, String> GET_ORIGINAL_ID = new Function<CustomFieldInfo, String>()
    {
        @Override
        public String apply(final CustomFieldInfo input)
        {
            return input.getOriginalId();
        }
    };

    private static final long serialVersionUID = 5388956019740247364L;
    private final String originalId;
    private final String name;
    private final String description;
    private final String typeModuleKey;
    private final String searcherModuleKey;
    private final List<OptionInfo> options;
    private final String pluginKey;

    @JsonCreator
    public CustomFieldInfo(
            @JsonProperty("originalId") String originalId,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("typeModuleKey") String typeModuleKey,
            @JsonProperty("searcherModuleKey") String searcherModuleKey,
            @JsonProperty("options") List<OptionInfo> options,
            @JsonProperty("pluginKey") String pluginKey)
    {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.typeModuleKey = typeModuleKey;
        this.searcherModuleKey = searcherModuleKey;
        this.options = options;
        this.pluginKey = pluginKey;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getTypeModuleKey()
    {
        return typeModuleKey;
    }

    public String getSearcherModuleKey()
    {
        return searcherModuleKey;
    }

    public List<OptionInfo> getOptions()
    {
        return options;
    }

    public String getPluginKey()
    {
        return pluginKey;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(name).append(typeModuleKey).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof CustomFieldInfo)) { return false; }

        CustomFieldInfo info = (CustomFieldInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(),info.getOriginalId()).append(this.getName(), info.getName()).append(this.getTypeModuleKey(),info.getTypeModuleKey()).isEquals();
    }

    @Override
    public String toString()
    {
        return "CustomFieldInfo name:" + name + " type module key:" + typeModuleKey; //NON-NLS
    }
}
