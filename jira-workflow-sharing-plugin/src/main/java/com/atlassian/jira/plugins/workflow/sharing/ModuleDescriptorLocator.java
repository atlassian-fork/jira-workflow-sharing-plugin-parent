package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.plugin.ModuleDescriptor;

import java.util.Collection;

public interface ModuleDescriptorLocator
{

    Collection<ModuleDescriptor> getEnabledModuleDescriptorsByModuleClassname(String moduleClassname);

    Collection<ModuleDescriptor> getEnabledModuleDescriptorsByModuleType(String moduleType);
}
