package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class WorkflowLayoutKeyFinderImpl implements WorkflowLayoutKeyFinder
{
    @Override
    public String getActiveLayoutKey(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                    setWorkflowName(workflowName).
                    setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.LIVE).
                    build();
    }

    @Override
    public String getDraftLayoutKey(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                setWorkflowName(workflowName).
                setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.DRAFT).
                build();
    }

    @Override
    public String getActiveLayoutV2Key(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                setWorkflowName(workflowName).
                setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.LIVE).
                setVersion2(true).
                build();
    }

    @Override
    public String getDraftLayoutV2Key(String workflowName)
    {
        return WorkflowLayoutPropertyKeyBuilder.newBuilder().
                setWorkflowName(workflowName).
                setWorkflowState(WorkflowLayoutPropertyKeyBuilder.WorkflowState.DRAFT).
                setVersion2(true).
                build();
    }
}
