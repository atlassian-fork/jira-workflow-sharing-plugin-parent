package com.atlassian.jira.plugins.workflow.sharing.exporter.component;

import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.model.StatusInfo;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.Lists;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

@ExportAsService
@Component
public class WorkflowStatusExportHelperImpl implements WorkflowStatusExportHelper
{
    // These are the IDs of the JIRA system statuses, as referenced by the JIRA system workflow. see JIRA's startupdatabase.xml
    private static final List<String> SYSTEM_STATUSES = Lists.newArrayList("1","3","4","5","6");

    @Override
    public String getStatusesJson(List<Status> statuses) throws IOException
    {
        List<StatusInfo> statusInfoList = Lists.newArrayList();

        for(Status status : statuses)
        {
            if (SYSTEM_STATUSES.contains(status.getId()))
            {
                // Don't export system default status info.
                continue;
            }
            Long statusCategoryId = (null == status.getStatusCategory()) ? null : status.getStatusCategory().getId();
            statusInfoList.add(new StatusInfo(status.getId(), status.getName(), status.getDescription(), statusCategoryId));
        }

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,statusInfoList);

        return sw.toString();
    }
}
