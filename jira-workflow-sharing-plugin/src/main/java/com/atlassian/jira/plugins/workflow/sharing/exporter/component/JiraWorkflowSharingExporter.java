package com.atlassian.jira.plugins.workflow.sharing.exporter.component;

import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;

import java.io.File;

public interface JiraWorkflowSharingExporter {

    ExportResult exportActiveWorkflow(String name, String notes) throws CanNotCreateFileException;

    ExportResult exportDraftWorflow(String name, String notes) throws CanNotCreateFileException;

    public static class ExportResult
    {
        private final File exportedFile;
        private final String workflowNameAsFileName;

        public ExportResult(File exportedFile, String workflowNameAsFileName)
        {
            this.exportedFile = exportedFile;
            this.workflowNameAsFileName = workflowNameAsFileName;
        }

        public File getExportedFile()
        {
            return exportedFile;
        }

        public String getWorkflowNameAsFileName()
        {
            return workflowNameAsFileName;
        }

        public String getName()
        {
            return exportedFile.getName();
        }
    }
}