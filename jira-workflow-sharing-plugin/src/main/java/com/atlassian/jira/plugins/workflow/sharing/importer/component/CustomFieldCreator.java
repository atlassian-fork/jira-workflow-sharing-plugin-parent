package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import org.ofbiz.core.entity.GenericEntityException;

public interface CustomFieldCreator
{
    CustomField createCustomField(CustomFieldInfo fieldInfo) throws GenericEntityException;
    
    void removeCustomFieldAndSchemes(CustomField field) throws RemoveException;
}
