package com.atlassian.jira.plugins.workflow.sharing.importer.rest;

import com.atlassian.jira.plugins.workflow.sharing.pac.JWSPacClient;
import com.atlassian.jira.plugins.workflow.sharing.pac.JWSPacClientFactory;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.marketplace.client.model.PluginSummary;
import com.atlassian.upm.api.util.Option;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

@Path("/workflowbundles")
public class WorkflowBundlesResource
{
    private final JWSPacClientFactory clientFactory;

    public WorkflowBundlesResource(JWSPacClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
    }

    @Path("/summary/{offset}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkflowBundles(@PathParam("offset") Integer offset, @QueryParam("filter") String filter)
    {
        final Option<String> parsedFilter = StringUtils.isNotBlank(filter) ? Option.some(filter) : Option.<String>none();

        // Extract all of the plugins
        JWSPacClient client = clientFactory.getPacClient();
        Page<PluginSummary> pluginSummariesPage = client.getWorkflowBundleListByFilter(parsedFilter, offset);
        List<PluginSummary> plugins = new ArrayList<PluginSummary>();
        for (PluginSummary pluginSummary : pluginSummariesPage)
        {
            plugins.add(pluginSummary);
        }

        // See if there is a next page
        Option<PageReference<PluginSummary>> nextPage = pluginSummariesPage.getNext();
        Integer nextOffset = -1;
        if(nextPage.isDefined())
        {
            nextOffset = nextPage.get().getOffset();
        }

        return Response.ok(new PluginHolder(plugins, nextOffset)).build();
    }

    @Path("/details/{pluginKey}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBundleDetails(@PathParam("pluginKey") String pluginKey)
    {
        JWSPacClient client = clientFactory.getPacClient();
        Plugin plugin = client.getWorkflowBundleDetails(pluginKey);

        if(null != plugin)
        {
            return Response.ok(plugin).build();
        }
        else
        {
            return Response.ok("{}").build();
        }
    }
    
}
