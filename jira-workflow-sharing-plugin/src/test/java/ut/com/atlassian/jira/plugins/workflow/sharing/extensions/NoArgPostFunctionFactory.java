package ut.com.atlassian.jira.plugins.workflow.sharing.extensions;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import java.util.HashMap;
import java.util.Map;

public class NoArgPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory
{
    public NoArgPostFunctionFactory()
    {
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> stringObjectMap)
    {
        
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> stringObjectMap, AbstractDescriptor abstractDescriptor)
    {
        
        if (!(abstractDescriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> stringObjectMap, AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> stringObjectMap)
    {
        Map<String, String> params = new HashMap<String, String>();

        return params;
    }
}