package ut.com.atlassian.jira.plugins.workflow.sharing.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.license.util.StringUtils;
import com.atlassian.plugin.util.ClassLoaderUtils;

import com.google.common.base.Preconditions;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.loader.WorkflowLoader;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

public class WorkflowTestUtils
{
    private final WorkflowManager workflowManager;
    private final UserManager userManager;

    public WorkflowTestUtils(WorkflowManager workflowManager, UserManager userManager)
    {
        this.workflowManager = workflowManager;
        this.userManager = userManager;
    }

    public static File getTestWorkflowsDirectory()
    {
        URL url = ClassLoaderUtils.getResource(TestConstants.TEST_WORKFLOW_DIRECTORY, WorkflowTestUtils.class);
        Preconditions.checkNotNull(url, "Missing the test workflow directory for the plugin.");
        return new File(url.getFile());
    }

    public static File getWorkflowFile(String filename)
    {
        return new File(getTestWorkflowsDirectory(), filename);
    }
    
    public File saveNewWorkflowXml(String filename,String xml) throws IOException
    {
        File tmpDir = createTemporaryDirectory();
        File workflowFile = new File(tmpDir,filename);
        FileUtils.writeStringToFile(workflowFile,xml);
        
        return workflowFile;
    }
    
    public JiraWorkflow importWorkflowFromXmlFile(File xmlFile, String workflowName) throws IOException, SAXException, FactoryException
    {
        InputStream is = new FileInputStream(xmlFile);

        final WorkflowDescriptor workflowDescriptor = WorkflowLoader.load(is, true);
        final ConfigurableJiraWorkflow newWorkflow = new ConfigurableJiraWorkflow(workflowName, workflowDescriptor, workflowManager);

        workflowManager.createWorkflow(userManager.getUserObject("admin"), newWorkflow);

        return workflowManager.getWorkflow(workflowName);
    }
    
    public ConfigurableJiraWorkflow createNoopWorkflow(String workflowName,CustomField cf) throws IOException, SAXException, FactoryException
    {
        File originalFile = getWorkflowFile(TestConstants.NOOP_POST_FUNCTION_WF_FILE);
        String originalXml = FileUtils.readFileToString(originalFile);
        String newXml = StringUtils.replaceAll(originalXml, TestConstants.CUSTOMFIELD_ID_TOKEN, cf.getId());
        
        String filename = workflowName.replaceAll(TestConstants.CLEAN_FILENAME_PATTERN, "-");
        File newWorkflowFile = saveNewWorkflowXml(filename,newXml);

        return (ConfigurableJiraWorkflow) importWorkflowFromXmlFile(newWorkflowFile,workflowName);
    }

    public ConfigurableJiraWorkflow createUltraSimpleWorkflow(String workflowName) throws IOException, SAXException, FactoryException
    {
        File originalFile = getWorkflowFile(TestConstants.ULTRA_SIMPLE_WF_FILE);
        String originalXml = FileUtils.readFileToString(originalFile);

        String filename = workflowName.replaceAll(TestConstants.CLEAN_FILENAME_PATTERN, "-");
        File newWorkflowFile = saveNewWorkflowXml(filename,originalXml);

        return (ConfigurableJiraWorkflow) importWorkflowFromXmlFile(newWorkflowFile,workflowName);
    }

    public ConfigurableJiraWorkflow createWorkflowWithIllegalSystemPostFunction(String workflowName) throws IOException, SAXException, FactoryException
    {
        File originalFile = getWorkflowFile(TestConstants.ILLEGAL_SYSTEM_POST_FUNCTION_WF_FILE);
        String originalXml = FileUtils.readFileToString(originalFile);

        String filename = workflowName.replaceAll(TestConstants.CLEAN_FILENAME_PATTERN, "-");
        File newWorkflowFile = saveNewWorkflowXml(filename,originalXml);

        return (ConfigurableJiraWorkflow) importWorkflowFromXmlFile(newWorkflowFile,workflowName);
    }

    public ConfigurableJiraWorkflow createMyPostFunctionWorkflow(String workflowName) throws IOException, SAXException, FactoryException
    {
        File originalFile = getWorkflowFile(TestConstants.MY_POST_FUNCTION_WF_FILE);
        String originalXml = FileUtils.readFileToString(originalFile);

        String filename = workflowName.replaceAll(TestConstants.CLEAN_FILENAME_PATTERN, "-");
        File newWorkflowFile = saveNewWorkflowXml(filename,originalXml);

        return (ConfigurableJiraWorkflow) importWorkflowFromXmlFile(newWorkflowFile,workflowName);
    }

    public ConfigurableJiraWorkflow createWorkflowWithCustomScreen(String workflowName,FieldScreen screen) throws IOException, SAXException, FactoryException
    {
        File originalFile = getWorkflowFile(TestConstants.CUSTOM_SCREEN_WF_FILE);
        String originalXml = FileUtils.readFileToString(originalFile);
        String newXml = StringUtils.replaceAll(originalXml, TestConstants.SCREEN_ID_TOKEN, screen.getId().toString());

        String filename = workflowName.replaceAll(TestConstants.CLEAN_FILENAME_PATTERN, "-");
        File newWorkflowFile = saveNewWorkflowXml(filename,newXml);

        return (ConfigurableJiraWorkflow) importWorkflowFromXmlFile(newWorkflowFile,workflowName);
    }
    
    public void clean()
    {
        FileUtils.deleteQuietly(TestConstants.WORKFLOWS_TEMP_DIRECTORY);
        FileUtils.deleteQuietly(TestConstants.JWB_TEMP_DIRECTORY);
    }

    private File createTemporaryDirectory()
    {
        File tempDir = new File(TestConstants.WORKFLOWS_TEMP_DIRECTORY, "workflows-" + randomString(6));
        FileUtils.deleteQuietly(tempDir);
        tempDir.mkdirs();
        return tempDir;
    }

    private File createTemporaryJWBDirectory()
    {
        File tempDir = new File(TestConstants.JWB_TEMP_DIRECTORY, "jwbs-" + randomString(6));
        FileUtils.deleteQuietly(tempDir);
        tempDir.mkdirs();
        return tempDir;
    }

    /**
     * Generate a random string of characters - including numbers
     *
     * @param length the length of the string to return
     * @return a random string of characters
     */
    private String randomString(int length)
    {
        StringBuilder b = new StringBuilder(length);

        for (int i = 0; i < length; i++)
        {
            b.append(randomAlpha());
        }

        return b.toString();
    }

    /**
     * Generate a random character from the alphabet - either a-z or A-Z
     *
     * @return a random alphabetic character
     */
    private char randomAlpha()
    {
        int i = (int) (Math.random() * 52);

        if (i > 25)
        { return (char) (97 + i - 26); }
        else
        { return (char) (65 + i); }
    }

    public File unzipJWBFile(File jwbFile) throws IOException
    {
        File tempDir = createTemporaryJWBDirectory();
        ZipUtils.unzip(jwbFile,tempDir);
        
        return tempDir;
    }

    public static String readLayout(String name) throws IOException
    {
        URL layoutsDirUrl = ClassLoaderUtils.getResource(TestConstants.TEST_LAYOUTS_DIRECTORY, WorkflowTestUtils.class);
        Preconditions.checkNotNull(layoutsDirUrl, "Could not load the layouts directory.");
        File layoutsDirectory = new File(layoutsDirUrl.getFile());
        File layoutsFile = new File(layoutsDirectory, name);

        return FileUtils.readFileToString(layoutsFile);
    }
}
