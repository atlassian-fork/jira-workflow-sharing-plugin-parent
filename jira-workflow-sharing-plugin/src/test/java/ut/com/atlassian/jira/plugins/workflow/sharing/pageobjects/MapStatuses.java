package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;

import java.util.List;

public class MapStatuses extends WizardPage<ChooseName, Summary>
{
    @ElementBy(id = "status-for-6")
    private SelectElement closedStatusSelect;

    @ElementBy(id = "status-for-1")
    private SelectElement openStatusSelect;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/wfshare-import/map-statuses";
    }

    @Override
    protected Class<Summary> getNextClass()
    {
        return Summary.class;
    }

    @Override
    protected Class<ChooseName> getBackCass()
    {
        return ChooseName.class;
    }

    public SelectElement getClosedStatusSelect()
    {
        return closedStatusSelect;
    }

    public SelectElement getOpenStatusSelect()
    {
        return openStatusSelect;
    }

    public List<String> getOriginalStatusNames()
    {
        List<PageElement> originalStatusElements = elementFinder.findAll(By.className("original-status"));
        return Lists.transform(originalStatusElements.subList(1, originalStatusElements.size()), new Function<PageElement, String>()
        {
            @Override
            public String apply(PageElement element)
            {
                return element.getText();
            }
        });
    }
}
