package ut.com.atlassian.jira.plugins.workflow.sharing.utils;

import java.io.File;

public final class TestConstants
{
    public static final String JWSP_GROUPID = "com.atlassian.jira.plugins.workflow.sharing";
    public static final String JWSP_TEST_PLUGIN_KEY = JWSP_GROUPID + ".jira-workflow-sharing-plugin-tests";
    
    public static final String NOOP_POST_FUNCTION_CLASSNAME = "ut." + JWSP_GROUPID + ".extensions.NOOPCustomFieldPostFunction";
    
    public static final String NOOP_POST_FUNCTION_WF_FILE = "noopCFFunction-workflow.xml";
    public static final String CUSTOM_SCREEN_WF_FILE = "workflowWithCustomScreen.xml";
    public static final String ULTRA_SIMPLE_WF_FILE = "ultra-simple-workflow.xml";
    public static final String MY_POST_FUNCTION_WF_FILE = "my-postfunction-workflow.xml";
    public static final String ILLEGAL_SYSTEM_POST_FUNCTION_WF_FILE = "illegal-system-post-function-workflow.xml";
    
    public static final String MY_POST_FUNCTION_CLASS = "com.example.workflow.function.MyPostFunction";
    public static final String MY_POST_FUNCTION_DESCRIPTOR_CLASS = "com.atlassian.jira.plugin.workflow.WorkflowFunctionModuleDescriptor";
    public static final String MY_POST_FUNCTION_PLUGIN_KEY = JWSP_GROUPID + ".my-post-function";
    public static final String MY_POST_FUNCTION_PLUGIN_FILE = "simple-post-function-1.0.jar";
    
    public static final String TEXT_FIELD_CF_PLUGIN_KEY = "com.atlassian.jira.plugin.system.customfieldtypes";
    
    public static final String TEXT_FIELD_CF_KEY = TEXT_FIELD_CF_PLUGIN_KEY + ":textfield";
    public static final String TEXT_FIELD_CF_CLASS = "com.atlassian.jira.issue.customfields.impl.RenderableTextCFType";

    public static final String SIMPLE_CF_PLUGIN_KEY = JWSP_GROUPID + ".simple-customfield-plugin";
    public static final String SIMPLE_CF_PLUGIN_CF_KEY = SIMPLE_CF_PLUGIN_KEY + ":beer-style-cf";
    public static final String SIMPLE_CF_PLUGIN_FILE = "simple-customfield-plugin-1.1.jar";
    
    public static final String JUNK_CLASS = "i.do.not.exist.Junk";

    public static final File PLUGINS_TEMP_DIRECTORY = new File("target/plugins-temp");
    public static final String TEST_PLUGIN_DIRECTORY = "test-plugins";

    public static final File WORKFLOWS_TEMP_DIRECTORY = new File("target/workflows-temp");
    public static final String TEST_WORKFLOW_DIRECTORY = "test-workflow-xml";
    public static final String TEST_LAYOUTS_DIRECTORY = "layouts";

    public static final File JWB_TEMP_DIRECTORY = new File("target/workflows-temp");
    
    public static final String UPM_ROOT_RESOURCE = "/rest/plugins/1.0/";

    public static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";
    
    public static final String CUSTOMFIELD_ID_TOKEN = "%CUSTOMFIELDID%";
    public static final String SCREEN_ID_TOKEN = "%SCREENID%";

    public static final String TEST_JWB_DIRECTORY = "test-jwbs";
    public static final String SIMPLE_JWB_FILE = "simpleWorkflow.jwb";
    public static final String SIMPLE_JWB_DRAFT_FILE = "simpleWorkflow-draft.jwb";
    public static final String NOOP_FUNCTION_JWB_FILE = "noopFunctionWorkflow.jwb";
    public static final String CUSTOM_SCREEN_JWB_FILE = "customScreenWorkflow.jwb";
    public static final String MY_POST_FUNCTION_JWB_FILE = "myPostFunctionWorkflow.jwb";
    public static final String SCREENS_FIELDS_NOTES_JWB_FILE = "screensFieldsNotesWorkflow.jwb";
    public static final String WITH_LAYOUTS_JWB_FILE = "withLayouts.jwb";

}
