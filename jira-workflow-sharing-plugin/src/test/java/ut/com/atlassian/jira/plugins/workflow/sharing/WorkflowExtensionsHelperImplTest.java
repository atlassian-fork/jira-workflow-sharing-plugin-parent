package ut.com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.plugins.workflow.sharing.*;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.workflow.loader.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WorkflowExtensionsHelperImplTest
{
    @Mock
    private ModuleDescriptorLocator moduleDescriptorLocator;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private WorkflowService workflowService;
    @Mock
    private WorkflowScreensHelper workflowScreensHelper;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private WhitelistChecker whitelistChecker;

    @Mock
    private ConditionsDescriptor rootDescriptor;

    @Mock
    private ActionDescriptor action;

    @Mock
    private RestrictionDescriptor restriction;

    private WorkflowExtensionsHelperImpl workflowExtensionsHelper;

    @Before
    public void setUp()
    {
        workflowExtensionsHelper = new WorkflowExtensionsHelperImpl(moduleDescriptorLocator, jiraAuthenticationContext,
                workflowService, workflowScreensHelper, workflowManager, whitelistChecker);

        when(action.getRestriction()).thenReturn(restriction);
        when(restriction.getConditionsDescriptor()).thenReturn(rootDescriptor);
    }

    @Test
    public void discardConditionsLeavesAllIfNoneIsIllegal()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));

        when(whitelistChecker.isAllowed(any(String.class))).thenReturn(true);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(2, rootNested.size());
        assertEquals(simpleNestedCondition, rootNested.get(0));
        assertEquals(compositeNestedCondition, rootNested.get(1));

        assertEquals(2, ((ConditionsDescriptor) rootNested.get(1)).getConditions().size());
        assertEquals(subConditionOne, ((ConditionsDescriptor) rootNested.get(1)).getConditions().get(0));
        assertEquals(subConditionTwo, ((ConditionsDescriptor) rootNested.get(1)).getConditions().get(1));
    }

    @Test
    public void discardConditionsRemovesIllegal()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor simpleNestedConditionTwo = mock(ConditionDescriptor.class);
        when(simpleNestedConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));
        when(simpleNestedConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedConditionOne);
        rootNested.add(simpleNestedConditionTwo);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionThree = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        subConditions.add(subConditionThree);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class4"));
        when(subConditionThree.getArgs()).thenReturn(ImmutableMap.of("class.name", "class5"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(false);
        when(whitelistChecker.isAllowed("class2")).thenReturn(true);
        when(whitelistChecker.isAllowed("class3")).thenReturn(false);
        when(whitelistChecker.isAllowed("class4")).thenReturn(true);
        when(whitelistChecker.isAllowed("class5")).thenReturn(true);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(2, rootNested.size());
        assertEquals(simpleNestedConditionTwo, rootNested.get(0));
        assertEquals(compositeNestedCondition, rootNested.get(1));

        assertEquals(2, ((ConditionsDescriptor) rootNested.get(1)).getConditions().size());
        assertEquals(subConditionTwo, ((ConditionsDescriptor) rootNested.get(1)).getConditions().get(0));
        assertEquals(subConditionThree, ((ConditionsDescriptor) rootNested.get(1)).getConditions().get(1));
    }

    @Test
    public void discardConditionsRemovesEmptyComposites()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionThree = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        subConditions.add(subConditionThree);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));
        when(subConditionThree.getArgs()).thenReturn(ImmutableMap.of("class.name", "class4"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(true);
        when(whitelistChecker.isAllowed("class2")).thenReturn(false);
        when(whitelistChecker.isAllowed("class3")).thenReturn(false);
        when(whitelistChecker.isAllowed("class4")).thenReturn(false);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(1, rootNested.size());
        assertEquals(simpleNestedCondition, rootNested.get(0));
    }

    @Test
    public void discardConditionsMergesSingleChildrenOfCompositeToParent()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionThree = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        subConditions.add(subConditionThree);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));
        when(subConditionThree.getArgs()).thenReturn(ImmutableMap.of("class.name", "class4"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(true);
        when(whitelistChecker.isAllowed("class2")).thenReturn(true);
        when(whitelistChecker.isAllowed("class3")).thenReturn(false);
        when(whitelistChecker.isAllowed("class4")).thenReturn(false);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(2, rootNested.size());
        assertEquals(simpleNestedCondition, rootNested.get(0));
        assertEquals(subConditionOne, rootNested.get(1));
    }

    @Test
    public void discardConditionsMergesSingleChildrenOfCompositeToParentTwoLevels()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor simpleSubConditionOne = mock(ConditionDescriptor.class);
        ConditionsDescriptor compositeSubCondition = mock(ConditionsDescriptor.class);
        subConditions.add(simpleSubConditionOne);
        subConditions.add(compositeSubCondition);
        when(simpleSubConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));

        List<AbstractDescriptor> subConditionsLevelTwo = new ArrayList<AbstractDescriptor>();
        when(compositeSubCondition.getConditions()).thenReturn(subConditionsLevelTwo);

        ConditionDescriptor subSubConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subSubConditionTwo = mock(ConditionDescriptor.class);
        subConditionsLevelTwo.add(subSubConditionOne);
        subConditionsLevelTwo.add(subSubConditionTwo);
        when(subSubConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));
        when(subSubConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class4"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(true);
        when(whitelistChecker.isAllowed("class2")).thenReturn(false);
        when(whitelistChecker.isAllowed("class3")).thenReturn(false);
        when(whitelistChecker.isAllowed("class4")).thenReturn(true);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(2, rootNested.size());
        assertEquals(simpleNestedCondition, rootNested.get(0));
        assertEquals(subSubConditionTwo, rootNested.get(1));
    }

    @Test
    public void discardConditionsRemovedRootIfEmpty()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(false);
        when(whitelistChecker.isAllowed("class2")).thenReturn(false);
        when(whitelistChecker.isAllowed("class3")).thenReturn(false);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(0, rootNested.size());

        verify(action).setRestriction(null);
    }

    @Test
    public void discardConditionsReplacesRootIfItHasSingleCompositeChild()
    {
        List<AbstractDescriptor> rootNested = new ArrayList<AbstractDescriptor>();
        when(rootDescriptor.getConditions()).thenReturn(rootNested);

        ConditionDescriptor simpleNestedCondition = mock(ConditionDescriptor.class);
        when(simpleNestedCondition.getArgs()).thenReturn(ImmutableMap.of("class.name", "class1"));

        ConditionsDescriptor compositeNestedCondition = mock(ConditionsDescriptor.class);
        rootNested.add(simpleNestedCondition);
        rootNested.add(compositeNestedCondition);
        when(compositeNestedCondition.getType()).thenReturn("OR");

        List<AbstractDescriptor> subConditions = new ArrayList<AbstractDescriptor>();
        when(compositeNestedCondition.getConditions()).thenReturn(subConditions);

        ConditionDescriptor subConditionOne = mock(ConditionDescriptor.class);
        ConditionDescriptor subConditionTwo = mock(ConditionDescriptor.class);
        subConditions.add(subConditionOne);
        subConditions.add(subConditionTwo);
        when(subConditionOne.getArgs()).thenReturn(ImmutableMap.of("class.name", "class2"));
        when(subConditionTwo.getArgs()).thenReturn(ImmutableMap.of("class.name", "class3"));

        when(whitelistChecker.isAllowed("class1")).thenReturn(false);
        when(whitelistChecker.isAllowed("class2")).thenReturn(true);
        when(whitelistChecker.isAllowed("class3")).thenReturn(true);

        workflowExtensionsHelper.removeConditions(action, true, null);

        assertEquals(1, rootNested.size());
        assertEquals(compositeNestedCondition, rootNested.get(0));

        assertEquals(2, ((ConditionsDescriptor) rootNested.get(0)).getConditions().size());
        assertEquals(subConditionOne, ((ConditionsDescriptor) rootNested.get(0)).getConditions().get(0));
        assertEquals(subConditionTwo, ((ConditionsDescriptor) rootNested.get(0)).getConditions().get(1));

        verify(restriction).setConditionsDescriptor(compositeNestedCondition);
    }

}
