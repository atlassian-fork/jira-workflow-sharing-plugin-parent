package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class ChooseName extends WizardPage<SelectWorkflow, MapStatuses>
{
    @ElementBy(id = "wfShareWorkflowName")
    private PageElement nameInput;

    @Override
    public String getUrl()
    {
       return "/plugins/servlet/wfshare-import/set-workflow-name";
    }

    public String getName()
    {
        return nameInput.getValue();
    }

    public void setName(String name)
    {
        nameInput.type(name);
    }

    @Override
    protected Class<MapStatuses> getNextClass()
    {
        return MapStatuses.class;
    }

    @Override
    protected Class<SelectWorkflow> getBackCass()
    {
        return SelectWorkflow.class;
    }
}
