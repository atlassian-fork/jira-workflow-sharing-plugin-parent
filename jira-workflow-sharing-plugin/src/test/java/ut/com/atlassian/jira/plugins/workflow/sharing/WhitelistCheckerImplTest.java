package ut.com.atlassian.jira.plugins.workflow.sharing;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import com.atlassian.jira.plugins.workflow.sharing.WhitelistCheckerImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WhitelistCheckerImplTest
{
    private static final String ONE = "com.atlassian.ClassOne";
    private static final String TWO = "com.atlassian.ClassTwo";
    private static final String FOUR = "com.atlassian.ClassFour";

    private WhitelistCheckerImpl whitelistChecker;

    @Before
    public void setUp() throws IOException
    {
        whitelistChecker = new WhitelistCheckerImpl();
        whitelistChecker.readFromResource("/test-whitelist.txt");
    }

    @Test
    public void allowsPresentInWhitelistFile()
    {
        assertTrue(whitelistChecker.isAllowed(ONE));
        assertTrue(whitelistChecker.isAllowed(TWO));
    }

    @Test
    public void doesNotAllowNotPresentInWhitelistFile()
    {
        assertFalse(whitelistChecker.isAllowed(FOUR));
    }
}
