package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowLayoutKeyFinder;
import com.atlassian.jira.plugins.workflow.sharing.exporter.WorkflowExportNotesProvider;
import com.atlassian.jira.plugins.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.opensymphony.workflow.FactoryException;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.xml.sax.SAXException;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.PluginTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowSharingExporterTest
{
    private final JiraWorkflowSharingExporter jiraWorkflowSharingExporter;
    private final WorkflowTestUtils workflowTestUtils;
    private final WorkflowManager workflowManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserManager userManager;
    private final CustomFieldManager customFieldManager;
    private final ProjectManager projectManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final FieldScreenManager fieldScreenManager;
    private final PluginTestUtils pluginTestUtils;
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;
    private final WorkflowExportNotesProvider exportNotesProvider;
    private final WorkflowLayoutKeyFinder workflowLayoutKeyFinder;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;

    private CustomField builtInCF;
    private FieldScreen screenWithCustomField;

    public WorkflowSharingExporterTest(JiraWorkflowSharingExporter jiraWorkflowSharingExporter, WorkflowTestUtils workflowTestUtils,
                                       WorkflowManager workflowManager, JiraAuthenticationContext jiraAuthenticationContext, UserManager userManager,
                                       CustomFieldManager customFieldManager, ProjectManager projectManager, WorkflowSchemeManager workflowSchemeManager,
                                       FieldScreenManager fieldScreenManager, PluginTestUtils pluginTestUtils, PluginController pluginController,
                                       PluginAccessor pluginAccessor, WorkflowExportNotesProvider exportNotesProvider, WorkflowLayoutKeyFinder workflowLayoutKeyFinder,
                                       WorkflowDesignerPropertySet workflowDesignerPropertySet)
    {
        this.jiraWorkflowSharingExporter = jiraWorkflowSharingExporter;
        this.workflowTestUtils = workflowTestUtils;
        this.workflowManager = workflowManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userManager = userManager;
        this.customFieldManager = customFieldManager;
        this.projectManager = projectManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.fieldScreenManager = fieldScreenManager;
        this.pluginTestUtils = pluginTestUtils; 
        this.pluginController = pluginController; 
        this.pluginAccessor = pluginAccessor;
        this.exportNotesProvider = exportNotesProvider;
        this.workflowLayoutKeyFinder = workflowLayoutKeyFinder;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
    }

    @BeforeClass
    public void setupData() throws GenericEntityException
    {
        ApplicationUser user = userManager.getUserByName("admin");
        jiraAuthenticationContext.setLoggedInUser(user);

        this.builtInCF = createCustomField(TestConstants.TEXT_FIELD_CF_KEY, "myTextField");

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        screenWithCustomField = new FieldScreenImpl(fieldScreenManager);
        screenWithCustomField.setName("screenWithCustomField");
        screenWithCustomField.addTab("sometab");
        screenWithCustomField.getTab(0).addFieldScreenLayoutItem(builtInCF.getId());
    }

    @AfterClass
    public void removeData() throws RemoveException
    {
        fieldScreenManager.removeFieldScreen(screenWithCustomField.getId());
        customFieldManager.removeCustomField(builtInCF);
        workflowTestUtils.clean();
    }

    @Test
    public void simpleActiveWorkflowExports() throws Exception
    {
        final String workflowName = "simpleActiveWorkflowExports";
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow(workflowName);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, "");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            final String[] dirs = explodedDir.list();
            assertEquals("found files other than workflow.xml, that's bad!: Files: " + Arrays.toString(dirs), 1, dirs.length);
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void simpleActiveWorkflowWithNotesExports() throws Exception
    {
        final String workflowName = "simpleActiveWorkflowWithNotesExports";
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow(workflowName);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, "B flat");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");
            File notesFile = new File(explodedDir, "notes.md");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertTrue("notes file doesn't exist!", notesFile.exists());
            
            String notes = FileUtils.readFileToString(notesFile);
            assertEquals("wrong notes content", "B flat", notes);

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Ignore("JSEV-189")
    @Test
    public void simpleDraftWorkflowExports() throws Exception
    {
        final String workflowName = "simpleDraftWorkflowExports";
        ApplicationUser lead = userManager.getUserByName("admin");
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow(workflowName);

        workflowManager.overwriteActiveWorkflow(lead, workflowName);
        workflowManager.createDraftWorkflow(lead, workflowName);

        Project testProject = projectManager.createProject(lead,
                new ProjectCreationData.Builder()
                        .withName("Test Project")
                        .withKey("TST")
                        .withType(new ProjectTypeKey("business"))
                        .withDescription("project for testing")
                        .withLead(lead)
                        .withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                        .build()
        );
        Scheme workflowScheme = workflowSchemeManager.createSchemeObject("testWorkflowScheme","");
        try
        {
            GenericValue schemeGV = workflowSchemeManager.getScheme(workflowScheme.getId());
            for(IssueType issueType : testProject.getIssueTypes())
            {
                workflowSchemeManager.addWorkflowToScheme(schemeGV, workflowName,issueType.getId());
            }

            workflowSchemeManager.addSchemeToProject(testProject,workflowScheme);
            workflowSchemeManager.clearWorkflowCache();

            workflowManager.createDraftWorkflow(lead, workflowName);

            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow(workflowName, "");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            final String[] dirs = explodedDir.list();
            assertEquals("found files other than workflow.xml, that's bad!: Files: " + Arrays.toString(dirs), 1, dirs.length);

        }
        finally
        {
            workflowSchemeManager.removeSchemesFromProject(testProject);
            workflowSchemeManager.deleteScheme(workflowScheme.getId());
            workflowSchemeManager.clearWorkflowCache();
            projectManager.removeProject(testProject);
            workflowManager.deleteDraftWorkflow(workflowName);
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void missingActiveWorkflowReturnsNull() throws Exception
    {
        JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow("missingActiveWorkflowReturnsNull", "");
        assertNull("jwb file was NOT null!", result);
    }

    @Test
    public void missingDraftWorkflowReturnsNull() throws Exception
    {
        final String workflowName = "missingDraftWorkflowReturnsNull";
        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow(workflowName);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportDraftWorflow("missingDraftWorkflowReturnsNull", "");
            assertNull("jwb file was NOT null!", result);

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void workflowWithJustAPluginExports() throws Exception
    {
        JiraWorkflow jw = null;
        final String workflowName = "myPostFunctionWorkflow";

        try
        {
            File pluginFile = pluginTestUtils.getPluginFile(TestConstants.MY_POST_FUNCTION_PLUGIN_FILE);
            pluginTestUtils.uploadPluginViaUPM(pluginFile);

            jw = workflowTestUtils.createMyPostFunctionWorkflow(workflowName);

            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, " - my-post-function", "Transition: close issue",
                    "Plugin post functions removed from this transition", " - My Post Function (my-post-function)");
        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }
            
            Plugin plugin = pluginAccessor.getPlugin(TestConstants.MY_POST_FUNCTION_PLUGIN_KEY);
            
            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
            
        }
    }
    
    @Test
    public void workflowWithFunctionAndFieldExports() throws Exception
    {
        final String workflowName = "noopFunctionWorkflow";
        JiraWorkflow jw = workflowTestUtils.createNoopWorkflow(workflowName, builtInCF);

        try
        {
            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, " - Jira Workflow Sharing Plugin-tests", "Transition: close issue",
                    "Plugin post functions removed from this transition", " - NOOP CF Function (Jira Workflow Sharing Plugin-tests)");
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void workflowWithIllegalSystemPostFunctions() throws Exception
    {
        final String wokflowName = "illegalSystemPostFunctions";
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithIllegalSystemPostFunction(wokflowName);

        try
        {
            String notes = exportNotesProvider.createNotes(jw);
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(wokflowName, notes);
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());

            assertNotes(explodedDir, "Transition: close issue",
                    "System post functions removed from this transition", " - Update Issue Field",
                    "Custom event fired by Fire Event Function was replaced by the generic event.");
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    private void assertNotes(File explodedDir, String... textsToVerify) throws IOException
    {
        File notesFile = new File(explodedDir, "notes.md");

        assertTrue("notes file doesn't exist", notesFile.exists());

        StringBuilder sb = new StringBuilder();
        BufferedReader notesReader = null;
        try
        {
            notesReader = new BufferedReader(new FileReader(notesFile));

            String line;
            while ((line = notesReader.readLine()) != null)
            {
                sb.append(line).append("\n");
            }
        }
        finally
        {
            if (notesReader != null)
            {
                notesReader.close();
            }
        }

        String notes = sb.toString();

        assertFalse("notes are blank", StringUtils.isBlank(notes));

        for (String textToVerify : textsToVerify)
        {
            assertTrue("expected text '" + textToVerify + "' was not found in notes \n" + notes, notes.contains(textToVerify));
        }
    }

    @Test
    public void workflowWithScreenAndFieldExports() throws Exception
    {
        final String workflowName = "customScreenWorkflow";
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen(workflowName, screenWithCustomField);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, "");
            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File workflowXml = new File(explodedDir, "workflow.xml");
            File cfJson = new File(explodedDir, "customfields.json");
            File screenJson = new File(explodedDir, "screens.json");

            assertTrue("workflow xml doesn't exist!", workflowXml.exists());
            assertTrue("customfields json doesn't exist!", cfJson.exists());
            assertTrue("screens json doesn't exist!", screenJson.exists());

            //check custom fields
            ObjectMapper mapper = new ObjectMapper();
            List<CustomFieldInfo> cfInfoList = mapper.readValue(cfJson, new TypeReference<List<CustomFieldInfo>>() {});
            assertEquals("wrong number of customfields from json", 1, cfInfoList.size());
            assertEquals("wrong customfield", builtInCF.getId(), cfInfoList.get(0).getOriginalId());

            //check screens
            List<ScreenInfo> screenInfoList = mapper.readValue(screenJson, new TypeReference<List<ScreenInfo>>() {});
            assertEquals("wrong number of screens from json", 1, screenInfoList.size());
            assertEquals("wrong screen", screenWithCustomField.getId(), screenInfoList.get(0).getOriginalId());

        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    private CustomField createCustomField(String typeKey, String cfName) throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(typeKey);

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        return customFieldManager.createCustomField(cfName, "", cfType, null, Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
    }

    @Test
    public void exportsLayout() throws FactoryException, SAXException, IOException, CanNotCreateFileException
    {
        final String workflowName = "exportsLayout";
        final String layoutJson = WorkflowTestUtils.readLayout("layout.json");
        final String layoutV2Json = WorkflowTestUtils.readLayout("layout.v2.json");

        JiraWorkflow jw = workflowTestUtils.createUltraSimpleWorkflow(workflowName);

        workflowDesignerPropertySet.setProperty(workflowLayoutKeyFinder.getActiveLayoutKey(workflowName), layoutJson);
        workflowDesignerPropertySet.setProperty(workflowLayoutKeyFinder.getActiveLayoutV2Key(workflowName), layoutV2Json);

        try
        {
            JiraWorkflowSharingExporter.ExportResult result = jiraWorkflowSharingExporter.exportActiveWorkflow(workflowName, "");

            File jwbFile = result.getExportedFile();

            assertNotNull("jwb file was null!", jwbFile);

            File explodedDir = workflowTestUtils.unzipJWBFile(jwbFile);
            File layoutJsonFile = new File(explodedDir, "layout.json");
            File layoutV2JsonFile = new File(explodedDir, "layout.v2.json");

            assertTrue("layout.json doesn't exist!", layoutJsonFile.exists());
            assertTrue("layout.v2.json doesn't exist!", layoutV2JsonFile.exists());

            String exportedLayoutJson = FileUtils.readFileToString(layoutJsonFile);
            String exportedLayoutV2Json = FileUtils.readFileToString(layoutV2JsonFile);

            assertEquals(exportedLayoutJson, layoutJson);
            assertEquals(exportedLayoutV2Json, layoutV2Json);
        }
        finally
        {
            workflowDesignerPropertySet.removeProperty(workflowLayoutKeyFinder.getActiveLayoutKey(workflowName));
            workflowDesignerPropertySet.removeProperty(workflowLayoutKeyFinder.getActiveLayoutV2Key(workflowName));
            workflowManager.deleteWorkflow(jw);
        }
    }
}
