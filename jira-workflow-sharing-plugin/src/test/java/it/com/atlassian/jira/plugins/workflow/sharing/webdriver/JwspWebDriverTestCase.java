package it.com.atlassian.jira.plugins.workflow.sharing.webdriver;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.plugin.util.ClassLoaderUtils;

import org.apache.commons.io.IOUtils;

import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.WizardPage;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class JwspWebDriverTestCase extends BaseJiraWebTest
{

    protected void nonAdminCannotAccess(String url) throws UnsupportedEncodingException
    {
        jira.quickLogin("jiradev", "jiradev", DashboardPage.class);

        String destination = jira.getProductInstance().getBaseUrl() + url;

        jira.getTester().getDriver().navigate().to(destination);

        String destinationUrl = URLEncoder.encode(destination, "UTF-8");
        assertCurrentURL("/login.jsp?permissionViolation=true&os_destination=" + destinationUrl);
    }

    protected void assertCurrentURL(String expectedFragment)
    {
        assertCurrentURL(expectedFragment, true);
    }

    private void assertCurrentURL(String expectedFragment, boolean assertEquals)
    {
        assertUrl(expectedFragment, getCurrentUrl(), assertEquals);
    }

    protected void assertUrl(String expectedFragment, String url)
    {
        assertUrl(expectedFragment, url, true);
    }

    private void assertUrl(String expectedFragment, String url, boolean assertEquals)
    {
        String expected = jira.getProductInstance().getBaseUrl() + expectedFragment;
        if (assertEquals)
        {
            assertTrue(MessageFormat.format("URL {0} should start with {1}", url, expected), url.startsWith(expected));
        }
        else
        {
            assertFalse(MessageFormat.format("URL {0} shouldn't start with {1}", url, expected), url.startsWith(expected));
        }
    }

    protected String getCurrentUrl()
    {
        return jira.getTester().getDriver().getCurrentUrl();
    }

    protected void assertButtons(WizardPage<?, ?> page)
    {
        assertCancel(page);
        assertTrue(page.isBackButtonPresent());
        assertTrue(page.isNextButtonPresent());
    }

    protected void assertCancel(WizardPage page)
    {
        assertUrl(getExpectedFinishUrl(), page.getCancelUrl());
    }

    protected abstract String getExpectedFinishUrl();


    protected String getExpectedTestNotesForImport() throws IOException
    {
        return IOUtils.toString(ClassLoaderUtils.getResourceAsStream("test-notes.txt", getClass()));
    }

    protected String getExpectedTestNotesForExport() throws IOException
    {
        return IOUtils.toString(ClassLoaderUtils.getResourceAsStream("test-notes-export.txt", getClass()));
    }

}
