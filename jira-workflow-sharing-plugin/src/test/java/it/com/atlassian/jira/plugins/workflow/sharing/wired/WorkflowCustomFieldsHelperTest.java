package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.RenderableTextCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowCustomFieldsHelper;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.OptionInfo;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowCustomFieldsHelperTest
{
    private CustomField myTextField;
    private CustomField myTextField2;
    
    private final CustomFieldManager customFieldManager;
    private final WorkflowCustomFieldsHelper workflowCustomFieldsHelper;
    private final WorkflowTestUtils workflowTestUtils;
    
    public WorkflowCustomFieldsHelperTest(CustomFieldManager customFieldManager, WorkflowCustomFieldsHelper workflowCustomFieldsHelper, WorkflowTestUtils workflowTestUtils)
    {
        this.customFieldManager = customFieldManager;
        this.workflowCustomFieldsHelper = workflowCustomFieldsHelper;
        this.workflowTestUtils = workflowTestUtils;
    }

    @BeforeClass
    public void setupData() throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(TestConstants.TEXT_FIELD_CF_KEY);

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);
        
        this.myTextField = customFieldManager.createCustomField("myTextField","",cfType,null,Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
        this.myTextField2 = customFieldManager.createCustomField("myTextField2","",cfType,null,Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
    }
    
    @AfterClass
    public void removeData() throws RemoveException
    {
        customFieldManager.removeCustomField(myTextField);
        customFieldManager.removeCustomField(myTextField2);
        workflowTestUtils.clean();
    }
    
    @Test
    public void customFieldClassnameMatches() throws Exception
    {
        String classname = workflowCustomFieldsHelper.getCustomFieldTypeClassname(myTextField.getId());
        assertEquals(RenderableTextCFType.class.getName(),classname);
    }

    @Test
    public void badIdReturnsNullClassname() throws Exception
    {
        String classname = workflowCustomFieldsHelper.getCustomFieldTypeClassname("badid");
        assertNull("classname should have been null but was " + classname,classname);
    }

    @Test
    public void workflowWithoutCustomfieldAndAdditionalFieldsReturnsJsonArray() throws IOException
    {
        List<OptionInfo> options = Collections.<OptionInfo> emptyList();
        CustomFieldInfo expectedInfo = new CustomFieldInfo(myTextField.getId(),myTextField.getName(),myTextField.getDescription(),TestConstants.TEXT_FIELD_CF_KEY,"",options,TestConstants.TEXT_FIELD_CF_PLUGIN_KEY);
        
        String json = workflowCustomFieldsHelper.getCustomFieldsJson(Collections.singleton(myTextField.getId()));

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Type collectionType = new TypeToken<List<CustomFieldInfo>>(){}.getType();
        
        List<CustomFieldInfo> infoList = gson.fromJson(json,collectionType);
        
        assertFalse("infoList should not be empty", infoList.isEmpty());
        assertEquals(expectedInfo,infoList.get(0));
    }
}
