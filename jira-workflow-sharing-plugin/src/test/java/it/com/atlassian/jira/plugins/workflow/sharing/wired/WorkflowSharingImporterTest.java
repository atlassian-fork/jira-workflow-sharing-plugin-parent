package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowLayoutKeyFinder;
import com.atlassian.jira.plugins.workflow.sharing.importer.JiraWorkflowSharingImporter;
import com.atlassian.jira.plugins.workflow.sharing.importer.SharedWorkflowImportPlan;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowImporterFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;
import org.junit.Test;
import org.junit.runner.RunWith;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.ImportTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowSharingImporterTest
{
    private final ImportTestUtils importTestUtils;
    private final WorkflowImporterFactory workflowImporterFactory;
    private final WorkflowManager workflowManager;
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;
    private final FieldScreenManager fieldScreenManager;
    private final CustomFieldManager customFieldManager;
    private final OptionsManager optionsManager;
    private final WorkflowLayoutKeyFinder workflowLayoutKeyFinder;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;

    public WorkflowSharingImporterTest(ImportTestUtils importTestUtils, WorkflowImporterFactory workflowImporterFactory, WorkflowManager workflowManager,
                                       PluginController pluginController, PluginAccessor pluginAccessor, FieldScreenManager fieldScreenManager, CustomFieldManager customFieldManager,
                                       OptionsManager optionsManager, WorkflowLayoutKeyFinder workflowLayoutKeyFinder, WorkflowDesignerPropertySet workflowDesignerPropertySet)
    {
        this.importTestUtils = importTestUtils;
        this.workflowImporterFactory = workflowImporterFactory;
        this.workflowManager = workflowManager;
        this.pluginController = pluginController; 
        this.pluginAccessor = pluginAccessor;
        this.fieldScreenManager = fieldScreenManager;
        this.customFieldManager = customFieldManager;
        this.optionsManager = optionsManager;
        this.workflowLayoutKeyFinder = workflowLayoutKeyFinder;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
    }

    @Test
    public void simpleImportWorks() throws Exception
    {
        JiraWorkflow jw = null;
        try
        {
            JiraWorkflowSharingImporter importer = workflowImporterFactory.newImporter();
    
            SharedWorkflowImportPlan plan = importTestUtils.getBasePlanForSimpleWorkflow();
            plan.setWorkflowName("importedSimpleWorkflow");

            jw = importAndAssert(importer, plan, "importedSimpleWorkflow");
        } 
        finally 
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }
        }
    }

    private JiraWorkflow importAndAssert(JiraWorkflowSharingImporter importer, SharedWorkflowImportPlan plan, String workflowName) throws IOException
    {
        JiraWorkflow result = importer.importWorkflow(plan);
        JiraWorkflow jw = workflowManager.getWorkflow(workflowName);

        assertNotNull("import result was null", result);
        assertNotNull("imported workflow 'importedSimpleWorkflow' doesn't exist", jw);

        return jw;
    }

    @Test
    public void importWithPrivatePluginImports() throws Exception
    {
        JiraWorkflow jw = null;
        Plugin plugin = null;

        try
        {
            JiraWorkflowSharingImporter importer = workflowImporterFactory.newImporter();

            SharedWorkflowImportPlan plan = importTestUtils.getBasePlanForMyPostFunctionWorkflow();
            plan.setWorkflowName("importedPrivatePluginWorkflow");

            jw = importAndAssert(importer, plan, "importedPrivatePluginWorkflow");

            plugin = pluginAccessor.getPlugin(TestConstants.MY_POST_FUNCTION_PLUGIN_KEY);
            assertNull("plugin was installed: " + TestConstants.MY_POST_FUNCTION_PLUGIN_KEY,plugin);
            
        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }

            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    @Test
    public void pluginGetsRemoved() throws Exception
    {
        JiraWorkflow jw = null;
        Plugin plugin = null;

        try
        {
            JiraWorkflowSharingImporter importer = workflowImporterFactory.newImporter();

            SharedWorkflowImportPlan plan = importTestUtils.getBasePlanForMyPostFunctionWorkflow();
            plan.setWorkflowName("importedPrivatePluginWorkflow");

            jw = importAndAssert(importer, plan, "importedPrivatePluginWorkflow");

            for(ActionDescriptor action : jw.getAllActions())
            {
                if(action.getName().equals("close issue"))
                {
                    List<FunctionDescriptor> functions = action.getPostFunctions();
                    functions.addAll(action.getUnconditionalResult().getPostFunctions());

                    for(FunctionDescriptor function : functions)
                    {
                        assertFalse("found my post function but it should have been removed", TestConstants.MY_POST_FUNCTION_CLASS.equals(function.getArgs().get("class.name")));
                    }
                }
            }
            
            plugin = pluginAccessor.getPlugin(TestConstants.MY_POST_FUNCTION_PLUGIN_KEY);
            assertNull("plugin was installed: " + TestConstants.MY_POST_FUNCTION_PLUGIN_KEY, plugin);

        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }

            if(null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    @Test
    public void importWithScreenAndCustomField() throws Exception
    {
        JiraWorkflow jw = null;

        FieldScreen ourScreen = null;
        CustomField ourTextField = null;
        CustomField ourCascadingSelectField = null;
        
        try
        {
            JiraWorkflowSharingImporter importer = workflowImporterFactory.newImporter();

            SharedWorkflowImportPlan plan = importTestUtils.getBasePlanForCustomScreenWorkflow();
            plan.setWorkflowName("importedScreenWorkflow");

            jw = importAndAssert(importer, plan, "importedScreenWorkflow");

            //screenWithCustomField
            Collection<FieldScreen> screens = fieldScreenManager.getFieldScreens();
            
            for(FieldScreen screen : screens)
            {
                if("screenWithCustomField".equals(screen.getName()))
                {
                    ourScreen = screen;
                    break;
                }
            }
            
            assertNotNull("field screen not found",ourScreen);
            
            for(FieldScreenLayoutItem item : ourScreen.getTab(0).getFieldScreenLayoutItems())
            {
                CustomField field = customFieldManager.getCustomFieldObject(item.getFieldId());
                
                if (field != null)
                {
                    if ("myTextField".equals(field.getName()))
                    {
                        ourTextField = field;
                    }
                    else if ("myCascadingSelectField".equals(field.getName()))
                    {
                        ourCascadingSelectField = field;
                    }
                }
            }
            assertNotNull("custom text field not found", ourTextField);
            assertNotNull("custom cascading select field not found", ourCascadingSelectField);

            IssueContext issueContext = GlobalIssueContext.getInstance();
            FieldConfig fieldConfig = ourCascadingSelectField.getRelevantConfig(issueContext);
            Options options = optionsManager.getOptions(fieldConfig);
            assertEquals(2, options.size());

            com.atlassian.jira.issue.customfields.option.Option optionOne = options.get(0);
            assertEquals("1", optionOne.getValue());
            assertEquals(2, optionOne.getChildOptions().size());
            assertEquals("11", optionOne.getChildOptions().get(0).getValue());

            com.atlassian.jira.issue.customfields.option.Option optionTwo = options.get(1);
            assertEquals("2", optionTwo.getValue());
            assertEquals(2, optionTwo.getChildOptions().size());
            assertEquals("21", optionTwo.getChildOptions().get(0).getValue());
        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }
            if(null != ourScreen)
            {
                fieldScreenManager.removeFieldScreen(ourScreen.getId());
            }
            
            if (null != ourTextField)
            {
                customFieldManager.removeCustomField(ourTextField);
            }
            if (null != ourCascadingSelectField)
            {
                customFieldManager.removeCustomField(ourCascadingSelectField);
            }
        }
    }

    @Test
    public void importsLayout() throws Exception
    {
        JiraWorkflow jw = null;
        try
        {
            JiraWorkflowSharingImporter importer = workflowImporterFactory.newImporter();

            SharedWorkflowImportPlan plan = importTestUtils.getBasePlanForLayoutWorkflow();
            plan.setWorkflowName("workflowWithLayout");

            jw = importAndAssert(importer, plan, "workflowWithLayout");

            String layoutJson = workflowDesignerPropertySet.getProperty(workflowLayoutKeyFinder.getActiveLayoutKey("workflowWithLayout"));
            String layoutV2Json = workflowDesignerPropertySet.getProperty(workflowLayoutKeyFinder.getActiveLayoutV2Key("workflowWithLayout"));

            String expectedLayoutJson = WorkflowTestUtils.readLayout("layout.json");
            String expectedLayoutV2Json = WorkflowTestUtils.readLayout("layout.v2.json");

            assertEquals(layoutJson, expectedLayoutJson);
            assertEquals(layoutV2Json, expectedLayoutV2Json);
        }
        finally
        {
            if(null != jw)
            {
                workflowManager.deleteWorkflow(jw);
            }
        }
    }
}
